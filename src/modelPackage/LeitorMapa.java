package modelPackage;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class LeitorMapa {
	public static final int X=10;
	public static final int Y=10;
	private String[] mapa = new String [X];
	private int[][] mapaPosicoes = new int [X][Y];
	
	public LeitorMapa() {
		try {
		String nomeDoArquivo = "maps/map_1.txt";
		FileReader arq = new FileReader(nomeDoArquivo);
		BufferedReader lerArq = new BufferedReader(arq);
		
		//lendo cabealho
		String linha = lerArq.readLine();
		linha = lerArq.readLine();
		linha = lerArq.readLine();
		
		
		for(int i = 0; i <= 9; i++) {
			linha = lerArq.readLine();
			mapa[i] = linha;
		}
		
		arq.close();
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public int getPositionValue(int x, int y) {
		return mapaPosicoes[x][y];
	}
	
}

