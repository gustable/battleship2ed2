package viewPackage;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import modelPackage.Jogador;

public class TelaJogoJFrame extends JFrame {

	private JPanel contentPane;
	PositionBtn btn[][] = new PositionBtn[10][10];

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaJogoJFrame frame = new TelaJogoJFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaJogoJFrame(){
		super("Batalha Naval 2.0");
		
		Jogador player = new Jogador();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1000,800);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(0,0,0,0));
		contentPane.setLayout(new GridLayout(11,10));
		setContentPane(contentPane);
		
		for(int i = 0; i <= 9; i++) {
			for(int j = 0; j <= 9; j++) {
				btn[i][j] = new PositionBtn();
				btn[i][j].setPreferredSize(new Dimension(50, 50));
				contentPane.add(btn[i][j]);
				btn[i][j].setCoordenadaX(i);
				btn[i][j].setCoordenadaY(j);
			}
		}
		
		JButton tiroSimples = new JButton("tiroSimples");
		contentPane.add(tiroSimples);
		
		JButton tiroHorizontal = new JButton("tiroHorizontal");
		contentPane.add(tiroHorizontal);
		
		JButton tiroVertical = new JButton("tiroVertical");
		contentPane.add(tiroVertical);
		
		JButton tiro2x2 = new JButton("tiro2x2");
		contentPane.add(tiro2x2);
		
		JLabel score = new JLabel("Score: " + player.getScore());
		contentPane.add(score);
	}

}
