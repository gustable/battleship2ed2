package viewPackage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import modelPackage.*;


public class PositionBtn extends JButton implements ActionListener {
	Jogador player = new Jogador();
	LeitorMapa mapa = new LeitorMapa();
	
	private int coordenadaX, coordenadaY;
	private int score = player.getResources();
	private int resources = player.getResources();
	ImageIcon erro, acerto;
	
	public PositionBtn() {
		setIcon(null);
		erro = new ImageIcon("images/sprite1.png");
		acerto = new ImageIcon("images/explosion.png");
		addActionListener(this);
		
	}
	
	public void actionPerformed(ActionEvent e) {
		if(mapa.getPositionValue(this.getCoordenadaX(),this.getCoordenadaY()) > 1) {
			score += 10;
			player.setScore(score);
			setIcon(acerto);
		}
		if(mapa.getPositionValue(this.getCoordenadaX(),this.getCoordenadaY()) == 0) {
			setIcon(erro);
		}
	}
	
	public int getCoordenadaX() {
		return this.coordenadaX;
	}
	public void setCoordenadaX(int coordenadaX) {
		this.coordenadaX = coordenadaX;
	}
	
	public int getCoordenadaY() {
		return this.coordenadaY;
	}
	public void setCoordenadaY(int coordenadaY) {
		this.coordenadaY = coordenadaY;
	}
	
	public void CalculadorDeRecursos(int tipoDeTiro) {
		switch(tipoDeTiro) {
			case 0: tipoDeTiro = 0;
				resources -= 5;
				player.setResources(resources);
				break;
			case 1: tipoDeTiro = 1;
				resources -= 25;
				player.setResources(resources);
				break;
			case 2: tipoDeTiro = 2;
				resources -= 25;
				player.setResources(resources);
				break;
			case 3: tipoDeTiro = 3;
				resources -= 15;
				player.setResources(resources);
				break;
		}
		
	}

}
