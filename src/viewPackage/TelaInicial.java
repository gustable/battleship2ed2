package viewPackage;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import javax.swing.JRadioButton;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.*;

import modelPackage.Jogador;
import java.awt.GridLayout;

public class TelaInicial extends JFrame{

	private JPanel contentPane;
	private JTextField textField;
	private final Action action = new SwingAction();

	/**
	 * Inicia a aplica��o
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaInicial frame = new TelaInicial();
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Cria a tela inicial
	 */
	public TelaInicial(){
		/*
		 * Cria��o da janela
		 */
		super("Batalha Naval 2.0");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblBatalhaNaval = new JLabel("Batalha Naval 2.0");
		lblBatalhaNaval.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblBatalhaNaval);
		
		/*
		 * Inicializa��o dos objetos necess�rios para manipula��o dos dados
		 */
		Jogador player = new Jogador();
		
		/*
		 * Cria��o dos elementos internos da tela inicial
		 */
		JLabel lblNomeDoJogador = new JLabel("Nome do jogador");
		contentPane.add(lblNomeDoJogador);
		
		JTextField nomeDoJogador = new JTextField();
		contentPane.add(nomeDoJogador);
		nomeDoJogador.setColumns(10);
		
		JButton btnComecar = new JButton("COME\u00C7AR");
		btnComecar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				player.setNome(nomeDoJogador.getText());
				if(player.getNome().length() >= 2) {
					TelaJogoJFrame telaJogo = new TelaJogoJFrame();
					telaJogo.setVisible(true);
					
					dispose();
					
				} else if(player.getNome().length() < 2) {
					JOptionPane.showMessageDialog(contentPane, "� necess�rio que seu nome tenha, pelo menos, 2 caracteres");
					
				} else {
					JOptionPane.showMessageDialog(contentPane, "Algo inesperado aconteceu! Recarregue o jogo");
				}
			}
		});
		contentPane.add(btnComecar);
	}
	
	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "SwingAction");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}

}
